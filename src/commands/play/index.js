import command_player_move from './playerMove';

import { PLAYER_MOVE } from '../../../data/play_commands.toml'

export default {
	[PLAYER_MOVE]: command_player_move,
}

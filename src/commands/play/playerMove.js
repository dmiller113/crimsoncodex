export default (command, screen) => {
	const { entities, gameMap } = screen.state;
	const { offset } = command.args;

	gameMap.moveEntity(entities.player, offset);
}

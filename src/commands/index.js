import { SCREEN_UPDATE } from '../../data/core_commands.toml'
import command_screen_update from './core/screenUpdate';

export const commandsByConstant = {
	[SCREEN_UPDATE]: command_screen_update,
};

export default commandsByConstant;

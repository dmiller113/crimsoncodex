export const command_screen_update = (game, command) => {
  game.currentScreen = command.args.screen;
}

export default command_screen_update;

import { FOV } from 'rot-js';

import mapConstrain from './utils/constrain';

import { tiles } from '../data/tiles.toml';

const positionToString = ({ x, y }) => `${x}:${y}`;
const xToPositionValue = (acc, [value, objValue]) => {
	let positions = objValue;
	if (!Array.isArray(objValue)) positions = [objValue];
	positions.forEach(position => acc[positionToString(position)] = value);
	return acc;
}
const stringToPosition = string => {
	const [x, y] = string.split(':');
	return { x: parseInt(x, 10), y: parseInt(y, 10) };
}

export default class GameMap {
	constructor({
		mapOptions = {},
		entitiesToPosition = {},
		physicalRef,
		tileIDsToPositions = {},
	}) {
		this.mapOptions = mapOptions;
		this.positionsToEntity = Object
			.entries(entitiesToPosition)
			.reduce(xToPositionValue, {});
		this.positionsToTileID = Object
			.entries(tileIDsToPositions)
			.reduce(xToPositionValue, {});
		this.physicalRef = physicalRef;
		this.fovMap = new FOV.PreciseShadowcasting(this.fovInputFunction.bind(this));
		this.explored = {};
		this.fov = {};
		this.fovDirty = true;
	}
	thingProp(thing, prop) {
		if(thing.glyph) {
			return prop ? thing[prop] : thing;
		}
		return prop ? this.physicalRef[thing].state[prop] : this.physicalRef[thing].state;
	}
	getEntity(position) {
		return this.positionsToEntity[positionToString(position)];
	}
	// getTile : Map -> Position -> Entity
	getTile(position) {
		const tileID = this.positionsToTileID[positionToString(position)];
		return tiles[tileID];
	}
	// getAtPosition : Map -> Position -> (Either Entity Tile)
	getAtPosition(position) {
		const entity = this.getEntity(position);
		if (entity) return entity;
		return this.getTile(position);
	}
	// getPositionOfEntity : Map -> Entity -> (Maybe Position)
	getPositionOfEntity(entity) {
		return Object
			.entries(this.positionsToEntity)
			.reduce((acc, [positionString, dEntity]) => {
				if (acc !== null) {
					return acc;
				}
				if (dEntity === String(entity)) {
					return stringToPosition(positionString);
				}
				return acc;
			}, null)
	}
	// getGlyph : Map -> Position -> Glyph
	getGlyph(position) {
		const thing = this.getAtPosition(position);
		return this.thingProp(thing).glyph;
	}
	// isWalkable : Map -> Position -> (Bool Entity)
	isWalkable(position) {
		const thing = this.getAtPosition(position);
		return thing !== undefined && this.thingProp(thing).walkThrough;
	}
	// isSeeThrough : Map -> Position -> (Bool Entity)
	isSeeThrough(position) {
		const thing = this.getAtPosition(position);
		return thing !== undefined && this.thingProp(thing).seeThrough;
	}
	drawTiles(display) {
		Object.entries(this.positionsToTileID).forEach(([positionString, tileID]) => {
			const inFov = this.fov[positionString];
			const hasExplored = this.explored[positionString];
			if (!(inFov || hasExplored)) return;
			const {
				glyph: {
					bg: baseBg,
					fg: baseFg,
					symbol,
				},
			} = tiles[tileID];
			const { x, y } = stringToPosition(positionString);
			const fg = inFov ? baseFg : '#7d7d7d';
			const bg = inFov ? baseBg : '#000';

			display.draw(x, y, symbol, fg, bg);
		})
	}

	drawEntities(display) {
		Object.entries(this.positionsToEntity).forEach(([positionString, entity]) => {
			if (!(this.fov[positionString])) return;
			const {
				state: {
					glyph,
				}
			} = this.physicalRef[entity];

			glyph.draw(display, stringToPosition(positionString));
		})
	}

	draw(display, playerID) {
		if (this.fovDirty) {
			const { x, y } = this.getPositionOfEntity(playerID);
			this.fovMap.compute(x, y, 5, this.canSee.bind(this));
			this.fovDirty = false;
		}
		this.drawTiles(display);
		this.drawEntities(display);
	}
	removeEntityAtPosition(position, entity) {
		const positionString = positionToString(position);
		if (this.positionsToEntity[positionString] === String(entity)) {
			return delete this.positionsToEntity[positionString];
		}
		return false;
	}
	addEntityAtPosition(position, entity) {
		const positionString = positionToString(position);
		if (!this.positionsToEntity[positionString]) {
			this.positionsToEntity[positionString] = String(entity);
			return true;
		}
		return false;
	}
	// moveEntity : Map -> Entity -> Offset -> (Maybe Entity)
	moveEntity(entity, { x: cX, y: cY }) {
		const position = this.getPositionOfEntity(entity);
		const { x, y } = position;
		const [nX, nY] = mapConstrain(x + cX, y + cY);
		const newPosition = { x: nX, y: nY };

		const isWalkable = this.isWalkable(newPosition);
		if (isWalkable) {
			this.clearFov();
			this.removeEntityAtPosition(position, entity);
			this.addEntityAtPosition(newPosition, entity);
			return { result: true, collide: null };
		} else {
			const thing = this.getAtPosition(newPosition);
			return { result: false, collide: thing };
		}
	}
	fovInputFunction(x, y) {
		return this.isSeeThrough({ x, y })
	}
	clearFov() {
		this.fov = {};
		this.fovDirty = true;
	}
	canSee(x, y) {
		const positionString = positionToString({ x, y });
		this.explored[positionString] = true;
		this.fov[positionString] = true;
	}
}

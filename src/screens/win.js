import drawCentered from '../utils/drawCentered';
import Title from './title';

import { SCREEN_UPDATE } from '../../data/core_commands.toml';

export default {
  enter: () => {
		console.dir('entered');
	},
	exit: () => {
		console.dir('exited');
	},
	draw: (display) => {
		drawCentered(display, 11, '-- You Win --');
		drawCentered(display, 12, 'Any key to restart');
	},
	input: (type, e) => {
		if (type !== 'keydown') return;
		return {
			type: SCREEN_UPDATE,
			args: { screen: Title },
			shouldRefresh: true,
		};
	},
	update: (command, game) => {
		console.dir(command);
		console.dir(game);
	},
};

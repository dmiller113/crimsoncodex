import playCommands from '../commands/play';

import information from '../information';

export default (state) => ({
	state: {},
  enter() {
		this.state = {
			...this.state,
			...state,
		};
	},
	draw(display) {
		const { gameMap } = this.state;
		gameMap.draw(display, this.state.entities.player);
		information(display, this.state);
	},
	input(type, e) {
		const { keys } = this.state;
		if (type === 'keydown') {
			return keys(e.key);
		}
	},
	update(command, game) {
		const { type } = command;
		if (playCommands[type]) {
			playCommands[type](command, this, game);
		}
	},
});

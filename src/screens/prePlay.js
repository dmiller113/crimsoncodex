import Physical from '../components/physical';

import GameMap from '../map';

import { EntityContainer } from '../entities';
import keyParser from '../keys';

import drawCentered from '../utils/drawCentered';
import makeCommand from '../utils/makeCommand';

import Play from './play';


import generator from '../generator';

import { SCREEN_UPDATE } from '../../data/core_commands.toml'

export default {
  enter: (addCommand) => {
		const maps = generator();
		const physical = {
			0: new Physical(0, {
				bg: '#000',
				fg: '#cdcdcd',
				symbol: '@',
				seeThrough: true,
			}),
			1: new Physical(1, {
				bg: '#000',
				fg: '#F0DC82',
				symbol: '@',
				seeThrough: true,
			})
		};

		addCommand(makeCommand(
			SCREEN_UPDATE,
			true,
			{
				screen: Play({
					components: {
						physical,
					},
					entities: EntityContainer([1], 0),
					keys: keyParser('play'),
					gameMap: new GameMap({
						entitiesToPosition: { 0: { x: 40, y: 12 }, 1: { x: 39, y: 11 } },
						tileIDsToPositions: maps[0].tileIDsToPositions,
						physicalRef: physical,
					}),
				}),
			},
		))
	},
	draw: (display) => {
		drawCentered(display, 12, '--Loading--');
	},
};

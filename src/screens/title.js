import drawCentered from '../utils/drawCentered';

import PrePlay from './prePlay';

import { SCREEN_UPDATE } from '../../data/core_commands.toml';

export default {
	draw: (display) => {
		drawCentered(display, 11, '-- Crimson Codex --');
		drawCentered(display, 12, 'Any key to start');
	},
	input: (type, e) => {
		if (type !== 'keydown') return;
		return {
			type: SCREEN_UPDATE,
			args: { screen: PrePlay },
			shouldRefresh: true,
		};
	},
};

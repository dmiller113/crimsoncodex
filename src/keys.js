import { keys } from '../data/keys.toml';

export default screenName => key => keys[screenName][key];

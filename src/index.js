import { Display } from 'rot-js';

import {
	Game,
	initializeGame,
} from './game';

import {
	bg,
	fg,
	fontSize,
	height,
	width,
} from '../data/display_constants.toml';

const initializePage = () => {
	const display = new Display({
		bg,
		fg,
		fontSize,
		height,
		width,
	});

	const displayDiv = document.getElementById('rot-container');
	displayDiv.appendChild(display.getContainer());

	return display;
}

(() => {
	const display = initializePage();
	const game = Game({ display });
	const bindEventToScreen = (type) => {
		window.addEventListener(type, (e) => {
			if (game.currentScreen == null) return;
			game.input(type, e);
		})
	};

	bindEventToScreen('keydown');
	bindEventToScreen('keyup');
	initializeGame(game);
	game.refresh();
})()

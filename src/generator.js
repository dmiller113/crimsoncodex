import { Map, RNG } from 'rot-js';

import { levels as levelStructure } from '../data/level_structure.toml';
import { height, width } from '../data/map_constants.toml';

const wrapOutside = (tilesIDsToPosition, wallTiles) => {
	const generateTile = generateTiles(tilesIDsToPosition, () => {}, wallTiles, {x: 0, y: 0});
	for (let x = 0; x < width; x++) {
		generateTile(x, height - 1, 0);
		generateTile(x, 0, 0);
	}

	for (let y = 0; y < height; y++) {
		generateTile(width - 1, y, 0);
		generateTile(0, y, 0);
	}
}

const getTileFromRNG = (weights) => (roll) => weights
	.reduce((acc, item) => {
		let type, weight;
		if (typeof item === 'object') {
			({ type, weight } = item);
		} else {
			type = item;
			weight = (Math.floor(100 / weights.length) / 100);
		}
		const { type: resType, chance } = acc;
		if (resType !== 'error') return acc;
		if (roll <= (chance + weight)) return { type, chance };
		return { type: resType, chance: chance + weight };
	}, { type: 'error', chance: 0 } );

const generateTiles = (tiles, floorTiles, wallTiles, { x: cX , y: cY }) => (x, y, value) => {
	const generator = !value ? wallTiles : floorTiles;
	const chance = RNG.getUniform();
	const { type: tileID } = generator(chance);
	tiles[tileID] = (tiles[tileID] || []).concat({ x: cX + x, y: y + cY });
}

const generateCellular = (structure, seed) => {
	RNG.setSeed(seed);
	const { Cellular } = Map;
	const {
		floorTile,
		wallTile,
		args,
		args: { chance, connect }
	} = structure;
	const floorTiles = getTileFromRNG(floorTile);
	const wallTiles = getTileFromRNG(wallTile);
	const offset = { x: 1, y: 1 };

  // generate cellular 1|0 structure, then assign by chance the 1|0 with wall/floor
	const map = new Cellular(width - 2, height - 2, args);
	map.randomize(chance);
	map.create();
	map.create();
	const tileIDsToPositions = {};

	if (connect) {
		map.create()
		map.connect(generateTiles(tileIDsToPositions, floorTiles, wallTiles, offset), 1);
	} else {
		map.create(generateTiles(tileIDsToPositions, floorTiles, wallTiles, offset));
	}
  wrapOutside(tileIDsToPositions, wallTiles);
	return { tileIDsToPositions };
};

const generateTemple = (structure) => {
	return {};
};

const generateSplit = (structure) => {
	return {};
}

const generatorByType = {
	cell: generateCellular,
	split: generateSplit,
	temple: generateTemple,
}

// levelStructure -> Map
export default (seed = 113) => {
	return levelStructure.map( (structure) => {
		const { type } = structure;
		if (type.length === 1) {
			return generatorByType[type](structure, seed);
		}
		return {};
	});
}

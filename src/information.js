import displayConstants from '../data/display_constants.toml';

const drawFunctionByVariable = {
	"<ACTIVE_STATUSES>": state => "None",
	"<ATK>": state => "10",
	"<COMBO>": state => `[${'___'}]`,
	"<DEF>": state => "10",
	"<FINISHER>": state => "None",
	"<HP>": state => "20/20",
	"<NAME>": () => displayConstants.constants.NAME,
	"<WHEELINFO>": state => "RBY\nY@R\nBBR",
};

const drawSection = (display, state, startLine, startColumn) => (line, index) => {
	const { constants } = displayConstants;

	let xPosition = startColumn;
	line.forEach((item) => {
		let content = `${item}:`;
		let spacer = 1;
		if (constants[item]) {
			content = constants[item];
		} else if (drawFunctionByVariable[item]) {
			content = drawFunctionByVariable[item](state);
			spacer = 2;
		}

		display.drawText(xPosition, startLine + index, content);
		xPosition += content.length + spacer;
	})
}

export default (display, state) => {
	const {
		separatorContent,
		separatorLine,
		statsContent,
		statsColumn,
		statsLine,
		statusColumn,
		statusContent,
		statusLine,
		statusSeparatorColumn,
		wheelColumn,
		wheelContent,
		wheelLine,
		wheelSeparatorColumn,
	} = displayConstants;

	display.drawText(0, separatorLine, separatorContent);

	[1, 2, 3].forEach((idx) => {
		display.draw(statusSeparatorColumn, separatorLine + idx, '|');
		display.draw(wheelSeparatorColumn, separatorLine + idx, '|');
	});

	statsContent.forEach(drawSection(display, state, statsLine, statsColumn));
	statusContent.forEach(drawSection(display, state, statusLine, statusColumn));
	wheelContent.forEach(drawSection(display, state, wheelLine, wheelColumn));

}

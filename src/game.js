import Title from './screens/title';

import commandsByConstant from './commands';

export const Game = ({ display }) => ({
	commandBuffer: [],
	display,
	screens: [],
  get currentScreen() {
		return this.screens[0];
	},
	set currentScreen(screen) {
		if (this.currentScreen && this.currentScreen.exit) {
			this.currentScreen.exit(this.addCommand.bind(this));
		}
		this.screens = [screen].concat(this.screens.slice(1));
		if (screen && screen.enter) {
			screen.enter(this.addCommand.bind(this));
		}
	},
	addCommand(command) {
		this.commandBuffer = this.commandBuffer.concat(command);
		this.update();
	},
	input(type, event) {
		if (!this.currentScreen || !this.currentScreen.input) return;

		const newCommands = this.currentScreen.input(type, event);

		if (newCommands) {
			this.commandBuffer = this
				.commandBuffer
				.concat(newCommands);

			this.update();
		}
	},
	refresh(clear = true) {
		if (this.display == null) return;
		if (clear) this.display.clear();
		if (this.currentScreen !== null) {
			this.currentScreen.draw(this.display);
		}
	},
	update() {
		const game = this;
		const updateState = (command, commands) => {
			if (!command) return;
			const { type, shouldRefresh = false } = command;
			const [head, ...tail] = commands;

			let screenCommands = [];
			if (this.currentScreen.update) {
				screenCommands = this.currentScreen.update(command, game);
			}

			let gameCommands;
			if (commandsByConstant[type]) {
				gameCommands = commandsByConstant[type](game, command);
			}

			const newCommands = []
				.concat(screenCommands, gameCommands)
				.filter(i => i);

			if (shouldRefresh) this.refresh();

			updateState(head, newCommands.length ? tail.concat(newCommands) : tail);
		}

		const [head, ...tail] = this.commandBuffer;
		this.commandBuffer = [];
		updateState(head, tail);
	}
});

export const initializeGame = (game, {
	commandBuffer = [],
	screens = [Title],
} = {}) => {
	game.commandBuffer = commandBuffer;
	game.screens = screens;

	if (screens.length !== 0 &&
			screens[0].enter) {
		game.currentScreen.enter();
	}
}


export default {
	Game,
	initializeGame,
};

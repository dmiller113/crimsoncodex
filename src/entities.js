/*
/ Entity is the ID that is used as the key for several component stores.
/ EntityContainers are just iterables that hold an Entity for each item.
/
*/
export const EntityContainer = (entities = [], player) => ({
	entities,
	player,
	[Symbol.iterator]() {
		return this.__iter();
	},
	* __iter() {
		yield this.player;
		yield* this.entities;
	}
});

export default {
	EntityContainer,
}

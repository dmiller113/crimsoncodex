import { width } from '../../data/display_constants.toml';

export default (display, yPos, text) => {
	const xPos = (width - text.length)/2;
	display.drawText(xPos, yPos, text);
}

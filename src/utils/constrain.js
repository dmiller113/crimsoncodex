import { width, height } from '../../data/display_constants.toml';

export const constrain = (maxI, i) => Math.min(Math.max(0, i), maxI)

export default (x, y) => [constrain(width - 1, x), constrain(height - 1, y)];

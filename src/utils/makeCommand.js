export default (type, shouldRefresh = true, args) => ({
  type,
  shouldRefresh,
  args,
});

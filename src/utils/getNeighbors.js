import mapConstrain from './constrain';

// getNeighbors : Position -> List Positions
const getNeighbors = (position) => {
	const { x, y } = position;
  return [
		[-1, -1], [0, -1], [1, -1],
		[-1, 0], [1, 0],
		[-1, 1], [0, 1], [1, 1],
	].reduce((acc, [dx, dy]) => {
		const [nX, nY] = mapConstrain(dx + x, dy + y)
		if (nX === x && nY === y) return acc;
		if (acc.find(({ x: fX, y: fY }) => (fX === nX && fY === nY))) {
			return acc;
		}
		return acc.concat({ x: nX, y: nY });
	}, []);
};

export default getNeighbors;

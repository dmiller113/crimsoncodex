import Glyph from '../glyph';

import Component from './component';

export default class Physical extends Component {
	constructor(id,	{
		symbol = 'ϰ',
		fg = '#fff',
		bg = '#f00',
		seeThrough = false,
		walkThrough = false,
	}) {
		super(id, 'physics');
		this.state = {
			glyph: new Glyph({ symbol, fg, bg }),
			seeThrough,
			walkThrough,
		};
	}
}

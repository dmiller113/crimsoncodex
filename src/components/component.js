import capitalize from '../utils/capitalize';

export default class Component {
	constructor(id, type, handlers = []) {
		this.id = id;
		this.type = type;
		this.handlers = handlers;
	}

	message(e) {
		const { type } = event;
		const newEvent = this
			.handlers
			.reduce( (acc, handler) => {
				const { name } = handler;
				if (name === `on${capitalize(type)}`) {
					return handler(acc);
				}
				return acc;
			}, e);
		return newEvent;
	}
}

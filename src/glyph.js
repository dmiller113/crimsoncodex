export default class Glyph {
  constructor({ symbol, fg, bg, zIndex = -1 }) {
		this.symbol = symbol;
		this.fg = fg;
		this.bg = bg;
		this.zIndex = zIndex;
	}

	draw(display, position) {
		const { x, y } = position;
		display.draw(x, y, this.symbol, this.fg, this.bg);
	}
}

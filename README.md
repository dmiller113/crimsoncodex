### Crimson Codex
Coffee Break roguelike. Made for the 7drl game jam.

Journey through the perilous Cave of Origins, resting place of the forbidden Grimoire, the Crimson Codex. Left there when the famous adventurer, the Vermillion Knight fell in battle after succuessfully banishing the Demon of N'y'Rod.

const express = require('express');
const app = express();
const ip = process.env.IP || '0.0.0.0';
const port = process.env.PORT || 4000;

app.use(express.static('lib'));
app.use(express.static('public'));

console.dir(`Listening on ${ip}:${port}`);
app.listen(port, ip);

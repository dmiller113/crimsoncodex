const HtmlPlugin = require('html-webpack-plugin');

module.exports = {
	mode: process.env.NODE_ENV || 'development',
	entry: ['./src/index.js'],
	output: {
		filename: 'bundle.js',
		path: __dirname + '/public',
		publicPath: '/',
	},
	module: {
		rules: [{
			test: /\.js$/,
			use: 'babel-loader',
			exclude: /node_modules/,
		}, {
			test: /\.toml$/,
			use: 'toml-loader',
			exclude: /node_modules/,
		}],
	},
	plugins: [new HtmlPlugin({
		template: 'src/index.html',
		title: 'Crimson Codex',
	})],
	watch: true,
	watchOptions: {
		aggregateTimeout: 500,
		ignored: [/node_modules/, /dist/, /Ideas/,'**/*.md'],
		poll: 1000
	}
}
